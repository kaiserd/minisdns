#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use Net::DNS::Nameserver;
$|++;

sub reply_handler {
  my ($qname, $qclass, $qtype, $peerhost,$query,$conn) = @_;    # parameters
  my ($rcode, @ans, @auth, @add, %headermask);                  # return values

  $headermask{aa} = 1; # mark the answer as authoritie by setting the 'aa' flag


  #-----------------------------------
  #print info about the received query
  #-----------------------------------	
  say("====Begin Query Info " . $query->header->id. "======================================================");

  say("Received query from $peerhost to " . $conn->{sockhost});
  $query->print;

  say("====End Query Info " . $query->header->id. "========================================================");

  #--------------------------------------------------------------------
  #analyse query
  #--------------------------------------------------------------------

  $qname = lc($qname); #to lower case
  my ($ip, $method, $alias, $qbase);


  # new method
  if($qname =~ /^(\d+)\.((?:\w|-)+)\.(\w+\.\w+\.\w+)$/) {
    ($ip, $alias, $qbase) = ($1,$2,$3);
    $method = 'qc';
    $ip = join '.', unpack "C*", pack "H*", $ip; #convert from hex to dotted notation
    $rcode  = "NOERROR";
  }



  #query
  elsif($qname =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\.(a|ca|cca|ccx|na|nu|da|dx)\.((?:\w|-)+)\.(\w+\.\w+\.\w+)$/) {
    ($ip, $method, $alias, $qbase) = ($1,$2,$3,$4);
    $rcode  = "NOERROR";
  }
  #query for cc or d
  elsif($qname =~ /^(.*)\.(ccu|du)\.((?:\w|-)+)\.(\w+\.\w+\.\w+)$/) {
    ($ip, $method, $alias, $qbase) = ($1,$2,$3,$4);
    $rcode  = "NOERROR";
  }
  #query for echo domain A
  elsif($qname =~ /^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\.(00)\.(\w+\.\w+\.\w+)$/){
    ($ip, $method, $qbase) = ($1,$2,$3);
    $alias = "";
    $rcode  = "NOERROR";
  }
  #query for dname echo domain A
  elsif($qname =~ /00.(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\.00\.(\w+\.\w+\.\w+)$/){
    ($ip, $qbase) = ($1,$2);
    $method = "d00";
    $alias = "";
    $rcode  = "NOERROR";
  }
  else{
    say("received query does not have a valid format -> answer NXDOMAIN");
    $rcode = "NXDOMAIN";
    ($ip, $method, $alias, $qbase) = ('', '', '', '');
  }

  #--------------------------------------------------------------------
  #create answer
  #--------------------------------------------------------------------

  my $alias_domain       = $alias . '.' . $qbase;
  my $echo_domain        = $ip . '.00.' .  $qbase;
  my $dname_echo_domain  = '00.' . $ip . '.00.' .  $qbase;
  my ($ttl_a, $ttl_c)    = (10000, 10000);

  my $nameserver 		   = 'echodns.disy.inf.uni-konstanz.de';
  my $nameserver_IP      = '134.34.165.170';

  if($method eq '00') { #direct query for echo domain
    my $rr_a = new Net::DNS::RR(name => $qname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;
  }

  elsif($method eq 'd00') { #direct query for dname echo domain
    my $rr_a = new Net::DNS::RR(name => $qname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;
  }

  #new method qc 
  #Use only one cname but that cname is on the same zone level as the query
  elsif($method eq 'qc') {
    # create first cname resource record
    my $rr_cname1 = new Net::DNS::RR(name => $qname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => 'q.' . $alias_domain);
    push @ans, $rr_cname1;

    # create a resource record
    my $rr_a = new Net::DNS::RR(name => $rr_cname1->cname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;

    #authority nameserver
    my $rr_ns = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "NS",
      nsdname => $nameserver);
    push @auth, $rr_ns;

    #A for the auth nameserver
    my $rr_nsa = new Net::DNS::RR(name => $nameserver,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $nameserver_IP);
    push @add, $rr_nsa;
  }



  elsif($method eq 'a') {
    my $rr_a = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;	
  }

  elsif($method eq 'cca') {
    # create first cname resource record
    my $rr_cname1 = new Net::DNS::RR(name => $qname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $alias_domain);
    push @ans, $rr_cname1;

    # create second cname resource record
    my $rr_cname2 = new Net::DNS::RR(name => $rr_cname1->cname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $echo_domain);
    push @ans, $rr_cname2;

    # create a resource record
    my $rr_a = new Net::DNS::RR(name => $rr_cname2->cname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;
  }

  elsif($method eq 'ccx') {
    # create first cname resource record
    my $rr_cname1 = new Net::DNS::RR(name => $qname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $alias_domain);
    push @ans, $rr_cname1;

    # create second cname resource record
    my $rr_cname2 = new Net::DNS::RR(name => $rr_cname1->cname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $echo_domain);
    push @ans, $rr_cname2;
  }


  elsif($method eq 'ca') {
    # create first cname resource record
    my $rr_cname1 = new Net::DNS::RR(name => $qname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $alias_domain);
    push @ans, $rr_cname1;

    # create a resource record
    my $rr_a = new Net::DNS::RR(name => $rr_cname1->cname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;
  }



  elsif($method eq 'ccu') {
    # create first cname resource record
    my $rr_cname1 = new Net::DNS::RR(name => $qname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $alias_domain);
    push @ans, $rr_cname1;

    # create second cname resource record
    my $rr_cname2 = new Net::DNS::RR(name => $rr_cname1->cname,
      ttl => $ttl_c,
      class => "IN",
      type => "CNAME",
      cname => $ip . '.xy.');
    push @ans, $rr_cname2;
  }

  elsif($method eq 'na') {
    my $rr_ns = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "NS",
      nsdname => $echo_domain);
    push @auth, $rr_ns;

    my $rr_a = new Net::DNS::RR(name => $rr_ns->nsdname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @add, $rr_a;
  }

  elsif($method eq 'nu') {
    my $rr_ns = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "NS",
      nsdname => $ip . '.xy.');
    push @auth, $rr_ns;
  }

  elsif($method eq 'da') {
    my $rr_d = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "DNAME",
      dname => $dname_echo_domain);
    push @ans, $rr_d;

    my $rr_a = new Net::DNS::RR(name => $rr_d->dname,
      ttl => $ttl_a,
      class => "IN",
      type => "A",
      address => $ip);
    push @ans, $rr_a;
  }

  elsif($method eq 'dx') {
    my $rr_d = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "DNAME",
      dname => $dname_echo_domain);
    push @ans, $rr_d;
  }

  elsif($method eq 'du') {
    my $rr_d = new Net::DNS::RR(name => $alias_domain,
      ttl => $ttl_c,
      class => "IN",
      type => "DNAME",
      dname => '00.' . $ip . '.xy.');
    push @ans, $rr_d;
  }



  #--------------------------------------------------------------------
  # print answer
  #--------------------------------------------------------------------
  my $reply  = $query->reply();
  my $header = $reply->header;
  my $headermask = \%headermask;

  my $opcode  = $query->header->opcode;
  my $qdcount = $query->header->qdcount;

  $reply->{answer}     = [@ans];
  $reply->{authority}  = [@auth];
  $reply->{additional} = [@add];

  if (!defined($headermask)) {
    $header->ra(1);
    $header->ad(0);
  } else {
    $header->opcode( $headermask->{opcode} ) if $headermask->{opcode};

    $header->aa(1) if $headermask->{aa};
    $header->ra(1) if $headermask->{ra};
    $header->ad(1) if $headermask->{ad};
  }

  say("====Begin Answer Info " . $reply->header->id. "=====================================================");
  $reply->print;
  say("====End Answer Info " . $reply->header->id. "=======================================================");


  #--------------------------------------------------------------------
  # return reply
  #--------------------------------------------------------------------
  return ($rcode, \@ans, \@auth, \@add, \%headermask);
}

my $ns = new Net::DNS::Nameserver(
  #LocalAddr    => "134.34.165.173",
  #LocalPort    => 53,
  LocalPort    => 5300,
  ReplyHandler => \&reply_handler,
  Verbose	     => 0
) || die "couldn't create nameserver object\n";

$ns->main_loop;

